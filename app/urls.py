from django.urls import path

from app.views import *

urlpatterns = [

    path('regions/', RegionList.as_view(template_name="regions/index.html"), name='index'),

    path('regions/detail/<int:pk>', RegionDetail.as_view(template_name="regions/detail.html"), name='detail'),

    path('regions/create', RegionCreate.as_view(template_name="regions/create.html"), name='create'),

    path('regions/edit/<int:pk>', RegionUpdate.as_view(template_name="regions/edit.html"), name='update'),

    path('regions/delete/<int:pk>', RegionDelete.as_view(), name='delete'),

    path('municipalities/', MunicipalityList.as_view(template_name="municipalities/index.html"), name='m_index'),

    path('municipalities/detail/<int:pk>', MunicipalityDetail.as_view(template_name="municipalities/detail.html"), name='m_detail'),

    path('municipalities/create', MunicipalityCreate.as_view(template_name="municipalities/create.html"), name='m_create'),

    path('municipalities/edit/<int:pk>', MunicipalityUpdate.as_view(template_name="municipalities/edit.html"), name='m_update'),

    path('municipalities/delete/<int:pk>', MunicipalityDelete.as_view(), name='m_delete'),

    path('excel/calculate-column/', calculate_column, name='calculate_column'),

    path('excel/', index, name='excel'),
]
