$(document).ready( function () {
   $('#excel').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            async:true,
            type: "POST",
            dataType: "json",
            url:"/excel/calculate-column/",
            data: $("#excel").serialize(),
            statusCode: {
                200: function(data) {
                    alert("La columna es " + data.column);
                },
                500: function (data) {
                    alert('Algo salió mal, contacte a su administrador')
                }
            }
        });
    });
});