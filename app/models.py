from django.db import models
from talenta365.utils import CommonModel

ACTIVE = 'active'
INACTIVE = 'inactive'

STATUS = (
    (ACTIVE, 'activo'),
    (INACTIVE, 'inactivo')
)


class Region(CommonModel):
    name = models.CharField(max_length=60)


class Municipality(CommonModel):
    name = models.CharField(max_length=60)
    status = models.CharField(max_length=10, choices=STATUS, default=ACTIVE)
    region = models.ManyToManyField(
        Region, through='RegionMunicipality', through_fields=('municipality', 'region'), related_name='municipality'
    )


class RegionMunicipality(models.Model):
    class Meta:
        unique_together = ('region', 'municipality')

    region = models.ForeignKey(Region, on_delete=models.CASCADE, db_index=True, related_name='rel_region')
    municipality = models.ForeignKey(
        Municipality, on_delete=models.CASCADE, db_index=True, related_name='rel_municipality'
    )
