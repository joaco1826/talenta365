# Habilitamos el uso de mensajes en Django
from django.contrib import messages
# Habilitamos los mensajes para class-based views
from django.contrib.messages.views import SuccessMessageMixin
from django.http import JsonResponse
from django.shortcuts import render
# Nos sirve para redireccionar despues de una acción revertiendo patrones de expresiones regulares
from django.urls import reverse
# Instanciamos las vistas genéricas de Django
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Instanciamos el modelo 'Region' para poder usarlo en nuestras Vistas CRUD
from .models import Region, Municipality


# Habilitamos los formularios en Django


class RegionList(ListView):
    model = Region


class RegionCreate(SuccessMessageMixin, CreateView):
    model = Region  # Llamamos a la clase 'Region' que se encuentra en nuestro archivo 'models.py'
    form = Region  # Definimos nuestro formulario con el nombre de la clase o modelo 'Region'
    fields = "__all__"  # Le decimos a Django que muestre todos los campos de la tabla 'region' de nuestra Base de Datos
    success_message = 'Región Creada Correctamente !'  # Mostramos este Mensaje luego de Crear una Region

    # Redireccionamos a la página principal luego de crear una region
    def get_success_url(self):
        return reverse('index')  # Redireccionamos a la vista principal 'index'


class RegionDetail(DetailView):
    model = Region


class RegionUpdate(SuccessMessageMixin, UpdateView):
    model = Region  # Llamamos a la clase 'Region' que se encuentra en nuestro archivo 'models.py'
    form = Region  # Definimos nuestro formulario con el nombre de la clase o modelo 'Region'
    fields = "__all__"  # Le decimos a Django que muestre todos los campos de la tabla 'region' de nuestra Base de Datos
    success_message = 'Región Actualizada Correctamente !'  # Mostramos este Mensaje luego de Editar una Region

    def __init__(self, *args, **kwargs):
        initial = kwargs.setdefault('initial', {})
        initial['municipality'] = Municipality.objects.all()
        return initial

    # Redireccionamos a la página principal luego de actualizar una region
    def get_success_url(self):
        return reverse('index')  # Redireccionamos a la vista principal 'index'


class RegionDelete(SuccessMessageMixin, DeleteView):
    model = Region
    form = Region
    fields = "__all__"

    # Redireccionamos a la página principal luego de eliminar una region
    def get_success_url(self):
        success_message = 'Region Eliminada Correctamente !'  # Mostramos este Mensaje luego de Eliminar una Region
        messages.success(self.request, (success_message))
        return reverse('index')  # Redireccionamos a la vista principal 'index'


class MunicipalityList(ListView):
    model = Municipality


class MunicipalityCreate(SuccessMessageMixin, CreateView):
    model = Municipality  # Llamamos a la clase 'Municipality' que se encuentra en nuestro archivo 'models.py'
    form = Municipality  # Definimos nuestro formulario con el nombre de la clase o modelo 'Municipality'
    fields = "__all__"  # Le decimos a Django que muestre todos los campos de la tabla 'municipality' de nuestra Base de Datos
    success_message = 'Municipio Creado Correctamente !'  # Mostramos este Mensaje luego de Crear una Municipio

    # Redireccionamos a la página principal luego de crear un municipio
    def get_success_url(self):
        return reverse('m_index')  # Redireccionamos a la vista principal 'index'


class MunicipalityDetail(DetailView):
    model = Municipality


class MunicipalityUpdate(SuccessMessageMixin, UpdateView):
    model = Municipality  # Llamamos a la clase 'Municipality' que se encuentra en nuestro archivo 'models.py'
    form = Municipality  # Definimos nuestro formulario con el nombre de la clase o modelo 'Municipality'
    fields = "__all__"  # Le decimos a Django que muestre todos los campos de la tabla 'municipality' de nuestra Base de Datos
    success_message = 'Municipio Actualizado Correctamente !'  # Mostramos este Mensaje luego de Editar una Municipio

    # Redireccionamos a la página principal luego de actualizar un municipio
    def get_success_url(self):
        return reverse('index')  # Redireccionamos a la vista principal 'index'


class MunicipalityDelete(SuccessMessageMixin, DeleteView):
    model = Municipality
    form = Municipality
    fields = "__all__"

    # Redireccionamos a la página principal luego de eliminar un municipio
    def get_success_url(self):
        success_message = 'Municipio Eliminado Correctamente!'  # Mostramos este Mensaje luego de Eliminar una Municipio
        messages.success(self.request, (success_message))
        return reverse('index')  # Redireccionamos a la vista principal 'index'


def calculate_column(request):
    if request.method == 'POST':
        array = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ',
            'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        ]
        number = int(request.POST.get("number", 1))
        list = len(array)
        result = array[1]
        if number > list:
            quotient = int(number / list)

            c = 1
            for i in range(1, quotient + 1):
                if i % list == 0:
                    result = 'A' + array[c]
                    c += 1
                    if c % list == 0:
                        result = 'AAA'
                else:
                    result = array[i]

            residue = number % list
            if residue == 0:
                residue = list
            result = result + array[residue - 1]
        else:
            result = array[number - 1]
        return JsonResponse({"column": result})


def index(request):
    return render(request, 'excel/index.html')
